// Require gulp
var gulp = require('gulp');

// Require plugins
var sass = require('gulp-sass');


// Init configurations
gulp.task('sass', function() {
	return gulp.src('app/scss/**/*.scss')
	  .pipe( sass().on('error', sass.logError) ) // Using gulp-sass
	  .pipe( gulp.dest('app/assets/css/') )
});


// Watch tasks
gulp.task('default', function() {

	gulp.watch('app/scss/**/*.scss', ['sass']);

});